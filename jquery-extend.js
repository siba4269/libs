!function ($) {
    "use strict";

    const oldShowHide = { 'show': $.fn.show, 'hide': $.fn.hide };
    const className = 'd-none';

    $.fn.extend({
        show: function () {
            this.each(function () {
                $(this).removeClass(className);
            });
            return oldShowHide.show.apply(this, arguments);
        },
        hide: function () {
            this.each(function () {
                $(this).addClass(className);
            });
            return oldShowHide.hide.apply(this, arguments);
        },
        onWith: function (selector, action) {
            if (typeof selector !== 'string') throw `${selector} must be a string`;
            if (!$.isPlainObject(action)) throw `${action} must be key value pair`;

            const $this = this;
            Object.entries(action).forEach(function ([eventName, fn]) {
                if (typeof eventName !== 'string' || !$.isFunction(fn)) {
                    throw `${eventName} must be be string & ${fn} must be a function`;
                }

                if(eventName == 'attributeChange') {

                    (function({ parent = document, mutationType, selector: _selector, fn }) {
                        _selector = _selector.split(',').map(i=>i.trim());
                        if(_selector.length == 0) return;

                        function mutationCallback(mutationList) {
                            mutationList.forEach(function(mutation) {

                                if(mutationType == mutation.type) {
                                    // const all = parent.querySelectorAll(_selector);
                                    // const $targets = [...all].filter(target => target == mutation.target);
                                    const $targets = $(selector).filter((i, target) => target == mutation.target);
                                    $targets.each( (i, t)=> fn.call(t, mutation) );
                                }

                                // switch(mutation.type) {
                                //     case "attributes":
                                //         console.log("Attribute name " + mutation.attributeName +
                                //             " changed to " + mutation.target[mutation.attributeName] +
                                //             " (was " + mutation.oldValue + ")", mutation);
                                //         break;
                                //     default:
                                //         console.log(mutation);
                                // }
                            });
                        }
                        
                        var observer = new MutationObserver(mutationCallback);
                        observer.observe(document, {
                            //childList: true,
                            attributes: true,
                            //characterData: false,
                            subtree: true,
                            //attributeFilter: ['max', 'min'],
                            attributeOldValue: true,
                            //characterDataOldValue: true
                        });
                    })({
                        mutationType: "attributes",
                        selector, fn
                    });

                } else {
                    $this.on(eventName, selector, fn);
                }
            });

            return $this;
        },
        reset: function () {
            return this.each(function () {
                if ($(this)[0].reset) $(this)[0].reset();
            });
        }
    });
}(window.jQuery);



$(document).onWith('input[only-number="true"]', {
    'input' : function () {
        if(isNaN(this.value)) {
            this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1').replace('.','');
        }

        if(isNaN(this.value) || this.value == '') return;

        let val = parseInt(this.value);
        const max = parseInt(this.getAttribute('max'));
        const min = parseInt(this.getAttribute('min'));
        const maxlength = parseInt(this.getAttribute('maxlength'));
        const minlength = parseInt(this.getAttribute('minlength'));

        //const maxError = this.getAttribute('max-error');
        const minError = this.getAttribute('min-error');
        //const maxlengthError = this.getAttribute('maxlength-error');
        const minlengthError = this.getAttribute('minlength-error');

        if(maxlength) {
            let isValueChanged = false; // this is to handle multiple 0
            while(val.toString().length > maxlength) {
                val = parseInt(val.toString().slice(0, -1));
                isValueChanged = true;
            }
            if(isValueChanged) {
                this.value = val;
            }
        }

        if(max) {
            let isValueChanged = false;
            while (val > max) {
                val = parseInt(val.toString().slice(0, -1));
                isValueChanged = true;
            }
            if(isValueChanged) {
                this.value = val;
            }
        }

        if(minlength) {
            if(val.length < minlength) {
                this.setCustomValidity(minlengthError || 'Invalid minlength value');
            } else {
                this.setCustomValidity('');
            }
        }

        if(min) {
            if(val < min) {
                this.setCustomValidity(minError || 'Invalid min value');
            } else {
                this.setCustomValidity('');
            }
        }
    },
    'mousewheel' : function(event) {
        if(!this.hasAttribute('is-beta')) return;

        if(isNaN(this.value)) return;

        const delta = event.originalEvent.deltaY;

        if (delta > 0) {
            this.value = val + 1;
        } else {
            if (val > 0) {
                this.value = val - 1;
            }
        }
    },
    'keyup' : function(event) {
        if(!this.hasAttribute('is-beta')) return;
        
        if(isNaN(this.value)) return;

        if (event.keyCode == '38') {
            this.value = val + 1;
        } else if (event.keyCode == '40') {
            this.value = val - 1;
        }

        $(this).trigger('input');
    }
});