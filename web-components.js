
class Container extends HTMLElement {
    /*
        <template id="test-tmp">
            <div>This is static test</div>
            <slot></slot>
            <slot name="footer"></slot>
        </template>
        <app-conainer template-id="test-tmp">
            <div slot="footer"> Hi!! </div>
            <div> hello!! </div>
        </app-conainer>
    */
    connectedCallback() {
        setTimeout(this.render.bind(this), 500);
    }

    render() {
        //debugger;
        const $this = this;

        const templateId = $this.getAttribute('template-id');
        //const hasEvent = [...$this.attributes].some(function({name}){ return name.startsWith('on') });
        const hasEvent = $this.getAttribute('event') == 'true';
        const template = document.getElementById(templateId || 'app-conainer').content.cloneNode(true);
        const slots = template.querySelectorAll('slot');

        //if(!$this._clone) {
        //    $this._clone = $this.cloneNode(true);
        //}            

        slots.forEach(function (slot) {
            const name = slot.getAttribute('name');
            if (name) {
                const content = $this.querySelectorAll('[slot="' + name + '"]');
                const cloned = [...content].map((s) => s.cloneNode(true));
                slot.replaceWith(...cloned);

            } else {
                const content = $this.querySelectorAll(':scope > *:not([slot])');
                console.log(content);
                const cloned = [...content].map((s) => s.cloneNode(true));
                slot.replaceWith(...cloned);
            }
        });

        const event = new Event('render', { bubbles: true });

        if (hasEvent) {
            $this.innerHTML = "";
            $this.appendChild(template);
            $this.dispatchEvent(event);
        } else {
            $this.replaceWith(template);
            document.dispatchEvent(event);
        }
    }
}
customElements.define('app-container', Container);