function timeSince(date) {
    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = seconds / 31536000;

    if (interval > 1) {
        return Math.floor(interval) + " years";
    }
    interval = seconds / 2592000;
    if (interval > 1) {
        return Math.floor(interval) + " months";
    }
    interval = seconds / 86400;
    if (interval > 1) {
        return Math.floor(interval) + " days";
    }
    interval = seconds / 3600;
    if (interval > 1) {
        return Math.floor(interval) + " hours";
    }
    interval = seconds / 60;
    if (interval > 1) {
        return Math.floor(interval) + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}
function formatDate(date) {
    if (!date) return { data: {}, html: '' };

    const d = new Date(date);

    const day = d.getDate();
    const month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][d.getMonth()];
    const year = d.getFullYear();

    function nth(d) {
        if (d > 3 && d < 21) return 'th';
        switch (d % 10) {
            case 1: return "st";
            case 2: return "nd";
            case 3: return "rd";
            default: return "th";
        }
    }
    const data = { day, sup: nth(day), month, year };
    return {
        data: data,
        html: day + '<sup>' + data.sup + '</sup>' + ' ' + month + ' ' + year
    };
}
function formatCurrency(number) {
    if (!number) return;
    return number.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}
function promisify(f) {
    return function (...args) { // return a wrapper-function (*)
        return new Promise(function (resolve, reject) {
            function callback(err, result) { // our custom callback for f (**)
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            }

            args.push(callback); // append our custom callback to the end of f arguments

            f.call(this, ...args); // call the original function
        });
    };
}
function scrollTo(el) {
    setTimeout(function () {
        if (!el) {
            return $("html, body").animate({ scrollTop: 0 });
        }
        const scrollPosition = $(el).offset().top;
        $("html, body").animate({ scrollTop: scrollPosition }, "slow");
    }, 0);
}
function debounce(func, wait, immediate) {
    let timeout;
    return function () {
        let context = this, args = arguments;
        let later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

function isFunction(functionToCheck) {
    return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

const isAdvancedUpload = function () {
    var div = document.createElement('div');
    return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

function activeDandD(selector) {
    if(isAdvancedUpload) {
        const els = document.querySelectorAll(selector);

        [...els].forEach(function(el){
            el.addEventListener('dragenter', function(e) { 
                e.preventDefault();
                e.stopPropagation();

                const el = e.currentTarget;
                
                if (el.contains(e.relatedTarget)) {
                    return;
                }
                
                if(el.classList.includes('disable-drop')) {
                    return;
                }
                
                //console.log('dragenter');
                el.classList.add('is-dragover'); 
            });
            el.addEventListener('dragleave', function(e){ 
                e.preventDefault();
                e.stopPropagation();

                const el = e.currentTarget;
                
                if (el.contains(e.relatedTarget)) {
                    return;
                }
                
                if(el.classList.includes('disable-drop')) {
                    return;
                }
                
                //console.log('dragleave');
                el.classList.remove('is-dragover');
            });
            el.addEventListener('dragleave', function(e){ 
                e.preventDefault();
                e.stopPropagation();
            });
            el.addEventListener('drop', function(e){
                const el = e.currentTarget;

                if(el.classList.includes('disable-drop')) {
                    return;
                }
                
                const fn = el.getAttribute('on-drop');
                if(!isFunction(fn)) {
                    throw 'Need on-drop function';
                }
                functions[fn](e);
            });
            el.addEventListener('drop', function(e){
                e.preventDefault();
                e.stopPropagation();

                const el = e.currentTarget;
                el.classList.remove('is-dragover');
                el.classList.add('disable-drop');
             });
        });
    }
}