
!function ($) {
    "use strict";

    $.fn.extend({
        serializeObject: function (options, fn = $.noop) {

            if (typeof options === "function") {
                fn = options;
                options = undefined;
            }

            // Default options
            options = options || { ignoreEmpty: true };

            let o = {};
            const $form = this;

            let a;
            if (options.ignoreEmpty) {
                a = $form.find(":input").filter((i, el) => $(el).val() != '').serializeArray();
            } else {
                a = $form.serializeArray();
            }

            $.each(a, function () {
                const { name, value } = this;
                const $el = $form.find('[name="' + name + '"]');
                const val = fn.call($el, { name, value });
                if (o[name] !== undefined) {
                    if (!o[name].push) {
                        o[name] = [o[name]];
                    }
                    o[name].push(val || value || '');
                } else {
                    o[name] = val || value || '';
                }
            });
            return o;
        }
    });
}(window.jQuery);