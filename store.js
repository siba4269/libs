// Memory storage plugin
(function () {    
    "use strict";
    addEventListener("storage", function (e) {
        const { key, newValue, oldValue } = e;
        const event = new CustomEvent('memory', { detail: Object.assign({ key }, _json({ newValue, oldValue })) });
        // trigger event for different window/tab 
        dispatchEvent(event);
    });

    const _oldSetItem = Storage.prototype.setItem;
    Storage.prototype.setItem = function (key, value) {
        let oldValue = this.getItem(key);
        _oldSetItem.apply(this, arguments);
        let newValue = this.getItem(key);

        const me = true; // If event trigger in same window/tab
        const event = new CustomEvent('memory', { detail: Object.assign({ key, me }, _json({ newValue, oldValue })) });
        // trigger event for same window/tab 
        dispatchEvent(event);
    }

    function _json({ newValue, oldValue }) {
        try {
            newValue = JSON.parse(newValue);
        } catch { }
        try {
            oldValue = JSON.parse(oldValue);
        } catch { }

        return { newValue, oldValue };
    }

    Storage.prototype.setObject = function (key, value) {
        this.setItem(key, JSON.stringify(value));
    }

    Storage.prototype.getObject = function (key) {
        var value = this.getItem(key);
        return value && JSON.parse(value);
    }
})();