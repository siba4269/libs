var __ObserveSelector = (selector, fn) => {
    const config  = {
        attributes: true, childList: true, subtree:true,
        attributeOldValue : true
    }
    const processElement = (el, mutation) => {
        if(typeof el.matches === "function" && el.matches(selector)) {                
            if(mutation.oldValue) {
                const newValue = el.getAttribute(mutation.attributeName);
                console.log('%cchanges: ', 'font-size: 120%; color: yellow' , mutation.attributeName, ' : ', mutation.oldValue, ' =>> ', newValue);
            }
            
            if(typeof fn === "function") {
                fn(mutation);
            }
        }
        
        // Add observer iframes
        if(el.nodeName == "IFRAME") {
            const iframeDoc = el.contentDocument || el.contentWindow.document;
            if(iframeDoc.body) {
                trigger(iframeDoc.body);
            } else {
                el.addEventListener("load", (e) => {
                    trigger(e.target.contentDocument.body);
                });                
            }
        } 
    };

    const callback = (mutations, observer) => {
        for(const mutation of mutations) {
          processElement(mutation.target, mutation); 
          
          for(const node of mutation.addedNodes) {
              processElement(node, mutation);
              
              if (node.childNodes) {
                [...node.childNodes].forEach(c => processElement(c, mutation));
              }
          }                  
        }
    };

    const trigger = (target) => {
        const observer = new MutationObserver(callback);    
        observer.observe(target, config);
    }
    trigger(document.body); 
};

//__ObserveSelector('#USER_ID', (mutation) => console.log(mutation));
